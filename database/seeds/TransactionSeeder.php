<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->insert([
            [
                'user_id' => 1,
                'customer_id' => 1,
                'account' => 764587857,
                'bank_branch' => 4345,
                'bank_num' => 10,
                'check_num' => 8881,
            ],
            [
                'user_id' => 2,
                'customer_id' => 2,
                'account' => 444537857,
                'bank_branch' => 32,
                'bank_num' => 11,
                'check_num' => 7561,
            ],
            [
                'user_id' => 3,
                'customer_id' => 3,
                'account' => 764999857,
                'bank_branch' => 555,
                'bank_num' => 12,
                'check_num' => 45671,
            ],
            [
                'user_id' => 4,
                'customer_id' => 4,
                'account' => 712347857,
                'bank_branch' => 456,
                'bank_num' => 10,
                'check_num' => 76481,
            ],
            [
                'user_id' => 1,
                'customer_id' => 1,
                'account' => 78537857,
                'bank_branch' => 675,
                'bank_num' => 11,
                'check_num' => 8731,
            ],
            [
                'user_id' => 2,
                'customer_id' => 2,
                'account' => 7699877857,
                'bank_branch' => 456,
                'bank_num' => 12,
                'check_num' => 45671,
            ],
            [
                'user_id' => 3,
                'customer_id' => 3,
                'account' => 76342357,
                'bank_branch' => 3456,
                'bank_num' => 20,
                'check_num' => 23451,
            ],
            [
                'user_id' => 4,
                'customer_id' => 4,
                'account' => 76543857,
                'bank_branch' => 321,
                'bank_num' => 22,
                'check_num' => 1456456,
            ],
            [
                'user_id' => 1,
                'customer_id' => 1,
                'account' => 76455457,
                'bank_branch' => 122,
                'bank_num' => 35,
                'check_num' => 1345345,
            ],
            [
                'user_id' => 2,
                'customer_id' => 2,
                'account' => 763454537857,
                'bank_branch' => 145,
                'bank_num' => 33,
                'check_num' => 1234234,
            ],
            [
                'user_id' => 3,
                'customer_id' => 3,
                'account' => 733545657,
                'bank_branch' => 1665,
                'bank_num' => 22,
                'check_num' => 122,
            ],
            [
                'user_id' => 4,
                'customer_id' => 2,
                'account' => 764778557,
                'bank_branch' => 1345,
                'bank_num' => 10,
                'check_num' => 154,
            ],
        ]);
    }
}
