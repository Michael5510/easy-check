@extends('layouts.app')

@section('title', 'Users')

@section('content')
    @if(Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <div class="table-header">
        <h1>רשימת עובדים</h1>
        <form method="post" action="{{url('users/search')}}">
            @csrf
            <div class="form-group row">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">
                        חפש
                    </button>
                </div>
                <div class="col-md-8">
                    <input id="search_string" type="text" class="form-control" name="search_string" required autocomplete="search_string">
                </div>

            </div>
        </form>
        <a class="btn btn-primary" href="{{url('/users/create')}}">הוסף עובד</a>
    </div>
    <table class="table">
        <tr>
            <th>שם</th>
            <th>כתובת דוא"ל</th>
            <th>מספר זהות</th>
            <th>תפקיד</th>
            <th>טלפון</th>
            <th>סניף</th>
        </tr>
        <!-- the table data -->
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->id_num}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle"
                                type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            {{$user->role === 'admin' ? 'מנהל' : 'עובד'}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               href="{{route('users.changerole',[$user->id, 'admin'])}}">מנהל</a>
                            <a class="dropdown-item"
                               href="{{route('users.changerole',[$user->id, 'user'])}}">עובד</a>
                        </div>
                    </div>
                </td>
                <td>{{$user->phone}}</td>
                <td>{{$user->branch->name}}</td>
                <td>
                    <a href="{{route('users.edit',$user->id)}}">ערוך</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{$users->links()}}
@endsection