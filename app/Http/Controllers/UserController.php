<?php

namespace App\Http\Controllers;

use App\Branch;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('admin');
        $users = User::paginate(10);
        return view('users.index', compact('users'));
    }

    public function indexSearch(Request $request)
    {
        $users = User::where('name', 'LIKE', "%$request->search_string%")
            ->orWhere('id_num', 'LIKE', "%$request->search_string%")
            ->orWhere('role', 'LIKE', "%$request->search_string%")
            ->orWhere('phone', 'LIKE', "%$request->search_string%")
            ->orWhere('email', 'LIKE', "%$request->search_string%")
            ->orWhereHas('branch', function ($q) use ($request) {
                $q->where('name', 'LIKE', "%$request->search_string%");
            })->paginate(10);
        return view('users.index', compact('users'));
    }

    public function changeRole($uid, $role)
    {
        Gate::authorize('change-role');
        $user = User::findOrFail($uid);
        $user->role = $role;
        $user->save();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('admin');
        $branches = Branch::All();
        return view('users.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('admin');
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->id_num = $request->id_num;
        $user->branch_id = $request->branch_id;
        $user->role = $request->role;
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('admin');
        $user = User::findOrFail($id);
        $branches = Branch::All();
        return view('users.edit', compact('user', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('admin');
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->id_num = $request->id_num;
        $user->branch_id = $request->branch_id;
        $user->role = $request->role;
        if ($request->password != null) {
            $user->password = Hash::make($request->password);
        }
        $user->save();
        return redirect('users');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('admin');
        $user = User::findOrFail($id);
        $transactions = $user->transactions;
        foreach ($transactions as $transaction) {
            $transaction->delete();
        }
        $user->delete();
        return redirect('users');
    }
}
