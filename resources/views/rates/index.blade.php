@extends('layouts.app')

@section('title', 'Rates')

@section('content')
    @if(Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <div class="table-header">
    <h1>List of rates</h1>
    <a class="btn btn-primary" href="{{url('/rates/updateall')}}"> Update Rates</a>
    </div>
    <table class="table">
        <tr>
            <th>id</th>
            <th>Symbol</th>
            <th>Rate to shekel</th>
            <th>Created</th>
            <th>Updated</th>
        </tr>
        <!-- the table data -->
        @foreach($rates as $rate)
            <tr>
                <td>{{$rate->id}}</td>
                <td>{{$rate->symbol}}</td>
                <td>{{$rate->rate_to_shekel}}</td>
                <td>{{$rate->created_at}}</td>
                <td>{{$rate->updated_at}}</td>
                <td>
                    <a href="{{route('rates.edit',$rate->id)}}">Edit</a>
                </td>
                <td>
                    <a href="{{route('rate.delete',$rate->id)}}">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{$rates->links()}}
@endsection