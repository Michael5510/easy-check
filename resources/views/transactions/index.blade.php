@extends('layouts.app')

@section('title', 'Transactions')

@section('content')
    @if(Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <div class="table-header">
        <h1>עסקאות</h1>

        <form method="post" action="{{url('transactions/search')}}">
            @csrf
            <div class="form-group row">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">
                        חפש
                    </button>
                </div>
                <div class="col-md-8">
                    <input id="search_string"
                           type="text"
                           class="form-control"
                           name="search_string"
                           required
                           autocomplete="search_string">
                </div>

            </div>
        </form>

        <a class="btn btn-primary" href="{{url('/transactions/create')}}">הוספת עסקה חדשה</a>
    </div>

    <table class="table">
        <tr>
            {{--                <th><input type="checkbox" id="checkAll"> בחר הכל</th>--}}
            <th>לקוח</th>
            <th>מספר זהות</th>
            <th><a href="{{url('/transactions/sorted/check_num')}}">מספר שיק</a></th>
            <th><a href="{{url('/transactions/sorted/bank_num')}}">בנק</a></th>
            <th><a href="{{url('/transactions/sorted/bank_branch')}}">סניף בנק</a></th>
            <th><a href="{{url('/transactions/sorted/account')}}">חשבון</a></th>
            <th><a href="{{url('/transactions/sorted/branch')}}">סניף</a></th>
            <th><a href="{{url('/transactions/sorted/created_at')}}">נוצר</a></th>
        </tr>
        <!-- the table data -->
        @foreach($transactions as $transaction)
            <tr>
                {{--                    <td>{{$transaction->id}}</td>--}}
                <td>{{$transaction->customer->name}}</td>
                <td>{{$transaction->customer->id_num}}</td>
                <td>{{$transaction->check_num}}</td>
                <td>{{$transaction->bank_num}}</td>
                <td>{{$transaction->bank_branch}}</td>
                <td>{{$transaction->account}}</td>
                <td>{{$transaction->user->branch->name}}</td>
                <td>{{$transaction->created_at ? $transaction->created_at->format('Y/m/d') : ''}}</td>
                <td>
                    <a href="{{route('transactions.edit',$transaction->id)}}">ערוך</a>
                </td>
                <td>
                    <a href="{{route('transaction.delete',$transaction->id)}}">מחק</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{$transactions->links()}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
      $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
      });
    </script>
@endsection