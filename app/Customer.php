<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name', 'age', 'phone', 'rate', 'branch_id', 'id_num',
    ];

    public function transactions(){
        return $this->hasMany('App\Transaction','customer_id');
    }

    public function branch(){
        return $this->belongsTo('App\Branch','branch_id');
    }
}
