<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name' => Str::random(10),
                'phone' => '0548391172',
                'id_num' => '058798654',
                'rate' => 'A',
                'branch_id' => '1',
                'age' => 32,
            ],
            [
                'name' => Str::random(10),
                'phone' => '0589234758',
                'id_num' => '058228654',
                'rate' => 'B',
                'branch_id' => '2',
                'age' => 22,
            ],
            [
                'name' => Str::random(10),
                'phone' => '0512328764',
                'id_num' => '058333654',
                'rate' => 'C',
                'branch_id' => '3',
                'age' => 37,
            ],
            [
                'name' => Str::random(10),
                'phone' => '0528333627',
                'id_num' => '058465654',
                'rate' => 'A',
                'branch_id' => '4',
                'age' => 72,
            ],
        ]);
    }
}
