<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('transactions');
});
Route::get('/home', function () {
    return redirect('transactions');
});

Auth::routes(['register' => false]);
Route::post('users/search', 'UserController@indexsearch')->name('users.search')->middleware('auth');
Route::resource('users', 'UserController')->middleware('auth');
Route::get('users/changerole/{uid}/{role}', 'UserController@changeRole')->name('users.changerole');

Route::get('customers/search', 'CustomerController@search')->name('customers.search')->middleware('auth');
Route::post('customers/index-search', 'CustomerController@indexsearch')->middleware('auth');
Route::get('customers/sorted/{by}', 'CustomerController@indexsorted')->name('customers.sorted')->middleware('auth');
Route::resource('customers', 'CustomerController')->middleware('auth');

Route::post('transactions/search', 'TransactionController@indexSearch')->middleware('auth');
Route::get('transactions/sorted/{by}', 'TransactionController@indexsorted')->name('transactions.sorted')->middleware('auth');
Route::get('transactions/filtered/{by}/{what}', 'TransactionController@indexfiltered')->name('transactions.filtered')->middleware('auth');
Route::resource('transactions', 'TransactionController')->middleware('auth');
Route::get('transactions/{id}/delete/', 'TransactionController@destroy')->name('transaction.delete')->middleware('auth');
Route::post('transactions/bulkdelete', 'TransactionController@bulkdelete')->middleware('auth');
