@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">ערוך עובד</div>

                    <div class="card-body">
                        <form method="POST" action="{{action('UserController@update', $user->id)}}">
                            @method('PATCH')
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('שם') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('כתובת דוא"ל') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="id_num" class="col-md-4 col-form-label text-md-right">{{ __('מספר זהות') }}</label>

                                <div class="col-md-6">
                                    <input id="id_num" type="text" class="form-control @error('id_num') is-invalid @enderror" name="id_num" value="{{$user->id_num}}" required autocomplete="id_num">

                                    @error('id_num')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('טלפון') }}</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$user->phone}}" required autocomplete="phone">

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('סיסמא') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('הזן סיסמא שנית') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="role" class="col-md-4 col-form-label text-md-right">{{__('תפקיד')}}</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="role">
                                            @if($user->role == 'admin')
                                                <option value="admin" selected="selected">
                                                    מנהל
                                                </option>
                                            @else
                                                <option value="admin">
                                                    מנהל
                                                </option>
                                            @endif
                                            @if($user->role == 'user')
                                                <option value="user" selected="selected">
                                                    עובד
                                                </option>
                                            @else
                                                <option value="user">
                                                    עובד
                                                </option>
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="branch_id" class="col-md-4 col-form-label text-md-right">סניף</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="branch_id">
                                        @foreach ($branches as $branch)
                                            @if($user->branch->id == $branch->id)
                                                <option value="{{ $branch->id }}" selected="selected">
                                                    {{ $branch->name }}
                                                </option>
                                            @else
                                                <option value="{{ $branch->id }}">
                                                    {{ $branch->name }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        עדכן
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection