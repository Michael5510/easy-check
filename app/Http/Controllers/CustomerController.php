<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::paginate(10);
        return view('customers.index', compact('customers'));
    }

    public function indexSearch(Request $request)
    {
        $customers = Customer::where('name', 'LIKE', "%$request->search_string%")
            ->orWhere('id_num', 'LIKE', "%$request->search_string%")
            ->orWhere('rate', 'LIKE', "%$request->search_string%")
            ->orWhere('phone', 'LIKE', "%$request->search_string%")
            ->orWhereHas('branch', function ($q) use ($request) {
                $q->where('name', 'LIKE', "%$request->search_string%");
            })->paginate(10);
        return view('customers.index', compact('customers'));
    }

    public function search(Request $request)
    {
        $customers = [];

        if($request->has('q')){
            $search = $request->q;
            $customers = Customer::select("id", "name")
                ->where('name', 'LIKE', "%$search%")
                ->orWhere('phone', 'LIKE', "%$search%")
                ->orWhere('id_num', 'LIKE', "%$search%")
                ->get();
        }

        return response()->json($customers);
    }

    public function indexsorted($by)
    {
        $customers = Customer::query()->orderBy($by)->paginate(10);
        return view('customers.index', compact('customers'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::all();
        return view('customers.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $customer->name = $request->name;
        $customer->age = $request->age;
        $customer->phone = $request->phone;
        $customer->id_num = $request->id_num;
        $customer->rate = $request->rate;
        $customer->branch_id = $request->branch_id;
        $customer->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        $branches = Branch::all();
        return view('customers.edit', compact('customer', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $customer->update($request->all());
        return redirect('customers');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $transactions = $customer->transactions;
        foreach ($transactions as $transaction) {
            $transaction->delete();
        }
        $customer->delete();
        return redirect('customers');
    }
}
