@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">עריכת לקוח</div>

                    <div class="card-body">
                        <form method="POST" action="{{action('CustomerController@update', $customer->id)}}">
                            @method('PATCH')
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('שם') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$customer->name}}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('גיל') }}</label>

                                <div class="col-md-6">
                                    <input id="age" type="age" class="form-control @error('age') is-invalid @enderror" name="age" value="{{$customer->age}}" required autocomplete="age">

                                    @error('age')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rate" class="col-md-4 col-form-label text-md-right">{{__('דירוג')}}</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="rate">
                                        <option value="A" {{ $customer->rate === 'A' ? 'selected="selected"' : ''}}>
                                            A
                                        </option>
                                        <option value="B" {{ $customer->rate === 'B' ? 'selected="selected"' : ''}}>
                                            B
                                        </option>
                                        <option value="C" {{ $customer->rate === 'C' ? 'selected="selected"' : ''}}>
                                            C
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="id_num" class="col-md-4 col-form-label text-md-right">{{ __('מספר זהות') }}</label>

                                <div class="col-md-6">
                                    <input id="id_num" type="text" class="form-control @error('id_num') is-invalid @enderror" name="id_num" value="{{$customer->id_num}}" required autocomplete="id_num">

                                    @error('id_num')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="branch_id" class="col-md-4 col-form-label text-md-right">סניף</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="branch_id">
                                        @foreach ($branches as $branch)
                                            @if($customer->branch->id == $branch->id)
                                                <option value="{{ $branch->id }}" selected="selected">
                                                    {{ $branch->name }}
                                                </option>
                                            @else
                                                <option value="{{ $branch->id }}">
                                                    {{ $branch->name }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('טלפון') }}</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$customer->phone}}" required autocomplete="phone">

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        עדכן
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection