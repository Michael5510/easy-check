<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        'name'
    ];

    public function users(){
        return $this->hasMany('App\User','branch_id');
    }

    public function customers(){
        return $this->hasMany('App\Customer','branch_id');
    }
}
