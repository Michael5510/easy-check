@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">יצירת עסקה</div>

                    <div class="card-body">
                        <form method="POST" action="{{action('TransactionController@store')}}">
                            @csrf
                            @if(auth()->user()->role === 'admin')
                                <div class="form-group row">
                                    <label for="user_id" class="col-md-4 col-form-label text-md-right">עובד</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="user_id">
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">
                                                    {{ $user->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="customer_id" class="col-md-4 col-form-label text-md-right">לקוח</label>
                                
                                <div class="col-md-6">
                                    <select class="livesearch form-control" name="customer_id"></select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="check_num" class="col-md-4 col-form-label text-md-right">{{ __('מספר שיק') }}</label>

                                <div class="col-md-6">
                                    <input id="check_num" type="text" class="form-control" name="check_num" required autocomplete="check_num">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_num" class="col-md-4 col-form-label text-md-right">{{ __('מספר בנק') }}</label>

                                <div class="col-md-6">
                                    <input id="bank_num" type="text" class="form-control" name="bank_num" required autocomplete="bank_num">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bank_branch" class="col-md-4 col-form-label text-md-right">{{ __('מספר סניף בנק') }}</label>

                                <div class="col-md-6">
                                    <input id="bank_branch" type="text" class="form-control" name="bank_branch" required autocomplete="bank_branch">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="account" class="col-md-4 col-form-label text-md-right">{{ __('מספר חשבון') }}</label>

                                <div class="col-md-6">
                                    <input id="account" type="text" class="form-control" name="account"  required autocomplete="account">
                                </div>
                            </div>

                          

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        צור
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script type="text/javascript">
      $('.livesearch').select2({
        placeholder: 'לקוח',
        ajax: {
          url: "{{route('customers.search')}}",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: $.map(data, function (item) {
                return {
                  text: item.name,
                  id: item.id
                }
              })
            };
          },
          cache: true
        }
      });
    </script>
@endsection