<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
            [
                'name' => 'פסגת זאב',
            ],
            [
                'name' => 'יפו',
            ],
            [
                'name' => 'מבשרת ציון',
            ],
            [
                'name' => 'תחנה מרכזית',
            ],
            [
                'name' => 'רמת אשכול',
            ],
        ]);
    }
}
