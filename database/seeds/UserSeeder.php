<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'id_num' => '039433263',
                'phone' => '0548394872',
                'role' => 'admin',
                'email' => 'a@a.com',
                'branch_id' => '1',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => Str::random(10),
                'id_num' => '039477726',
                'phone' => '0589384758',
                'role' => 'user',
                'email' => Str::random(10) . '@gmail.com',
                'branch_id' => '2',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => Str::random(10),
                'id_num' => '039456363',
                'phone' => '0503928764',
                'role' => 'user',
                'email' => Str::random(10) . '@gmail.com',
                'branch_id' => '3',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => Str::random(10),
                'id_num' => '039452263',
                'phone' => '0528374627',
                'role' => 'user',
                'email' => Str::random(10) . '@gmail.com',
                'branch_id' => '4',
                'password' => Hash::make('12345678'),
            ],
        ]);
    }
}
