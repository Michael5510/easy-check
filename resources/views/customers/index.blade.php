@extends('layouts.app')

@section('title', 'Customers')

@section('content')
    @if(Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <div class="table-header">
        <h1>רשימת לקוחות</h1>
        <form method="post" action="{{url('customers/index-search')}}">
            @csrf
            <div class="form-group row">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">
                        חפש
                    </button>
                </div>
                <div class="col-md-8">
                    <input id="search_string" type="text" class="form-control" name="search_string" required autocomplete="search_string">
                </div>

            </div>
        </form>
        <a class="btn btn-primary" href="{{url('/customers/create')}}"> הוספת לקוח</a>
    </div>
    <table class="table">
        <tr>
            <th><a href="{{url('/customers/sorted/name')}}">שם</a></th>
            <th><a href="{{url('/customers/sorted/id_num')}}">מספר זהות</a></th>
            <th><a href="{{url('/customers/sorted/phone')}}">טלפון</a></th>
            <th><a href="{{url('/customers/sorted/age')}}">גיל</a></th>
            <th><a href="{{url('/customers/sorted/age')}}">סניף</a></th>
            <th><a href="{{url('/customers/sorted/rate')}}">דירוג</a></th>
        </tr>
        <!-- the table data -->
        @foreach($customers as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->id_num}}</td>
                <td>{{$customer->phone}}</td>
                <td>{{$customer->age}}</td>
                <td>{{$customer->branch->name}}</td>
                <td>{{$customer->rate}}</td>
                <td>
                    <a href="{{route('customers.edit',$customer->id)}}">ערוך</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{$customers->links()}}
@endsection